#pragma once
#include <exception>
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <iostream>
#include <queue>
#include <deque>
#include <string>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <fstream>
#include <utility> 
#include "Helper.h"
#define MSG_SIZE 5000
#define TYPE_LEN 3
//TODO: make sure toSuicide is needed. (will the socket closing terminate the thread?)
class Server
{
	std::mutex mtxSocket;
	std::mutex mtx; //mtx for use to when accessing the shared queue, _msgs
	std::mutex mtxCv; //mtx for use with the condition variable cv
	std::condition_variable cv;//cv for notifying when a new msg has been added.
	std::deque<std::pair<std::string, int>> _users;
	//std::vector < bool> _toSuicide;
	std::deque<SOCKET> _sockets;
	std::queue<std::pair<std::string, int>> _msgs;
	std::string _data;
	void accept();
	SOCKET _serverSocket;
	std::string strSlicing(const std::string & str,int start, int end);//including start, excluding end
	void movToBack();//moves the front user to the back.
	void movSocketToBack();
	void sendOneOOneLastUser(SOCKET socket);//sends a 101(one o one) message to the first client
	void sendOneOOneSpecificUser(SOCKET socket, unsigned int loc);//sends a 101(one o one) message to a specific user. IMPORTANT: loc is the current location of the user in the queue. (so if the user is at first, pass 1 to the function)
	void sendOneOOneToAll();//sends a 101 (one o one) message to all users.
	void delUser(unsigned int userNum);
	void addMsgsOfClient(SOCKET clientSocket, unsigned int usrNum);
	void handleMsgs();
public:

	Server();
	~Server();
	void serve(int port);

};
