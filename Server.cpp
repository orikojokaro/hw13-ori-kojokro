#include "Server.h"

Server::Server()
{
	std::ifstream ifs("data.txt", std::ios::in);
	_data.assign((std::istreambuf_iterator<char>(ifs)),
		(std::istreambuf_iterator<char>()));
	ifs.close();
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 
	if (_serverSocket == INVALID_SOCKET)
	{
		printf("%d", GetLastError());
		throw std::exception(__FUNCTION__ " - socket");
	}
}

Server::~Server()
{
	unsigned int size = _users.size();
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		for (unsigned int i = 0; i < size; i++)
		{
			_users.erase(_users.begin() + i);
			closesocket(_sockets[i]);
		}
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;
	std::thread th1(&Server::handleMsgs, this);
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);
	_sockets.push_back(client_socket);
	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// the function that handles the conversation with the client
	std::thread th1(&Server::addMsgsOfClient, this, client_socket, _users.size());
	th1.detach();
}

std::string Server::strSlicing(const std::string& str, int start, int end)
{
	std::string res(end - start, 0);
	for (int i = start; i < end; i++)
	{
		res[i - start] = str[i];
	}
	return res;
}

void Server::movToBack()
{
	_users.push_back(_users.front());
	_users.pop_front();
}
void Server::movSocketToBack()
{
	_sockets.push_back(_sockets.front());
	_sockets.pop_front();
}
//sends a 101 message
void Server::sendOneOOneLastUser(SOCKET socket)
{
	if(_users.size() == 0)
	{
		throw (std::string("No users(1)"));
	}
	else if  (_users.size() > 1)
	{
		Helper::sendUpdateMessageToClient(socket, _data, _users.front().first, _users[_users.size() - 2].first, _users.size());
	}
	else
	{
		Helper::sendUpdateMessageToClient(socket, _data, _users.front().first, "", _users.size());
	}
}
// sends a 101(one o one) message to a specific user.
//IMPORTANT: loc is the current location of the user in the queue. (so if the user is first, pass 1 to the function)
void Server::sendOneOOneSpecificUser(SOCKET socket, unsigned int loc)
{
	if (_users.size() == 0)
	{
		throw (std::string("No users(1)"));
	}
	else if (_users.size() > 1)
	{
		Helper::sendUpdateMessageToClient(socket, _data, _users.front().first, _users[_users.size() - 2].first, loc + 1);
	}
	else
	{
		Helper::sendUpdateMessageToClient(socket, _data, _users.front().first, "", 1);//1 becuase if there is 1 user, he will always be first.
	}
}
//sends a 101 (one o one) message to all users.
void Server::sendOneOOneToAll()
{
	for (unsigned int i = 0; i < _users.size(); i++)
	{
		sendOneOOneSpecificUser(_sockets[i], i);
	}
}

void Server::delUser(unsigned int usrNum)
{	
	unsigned int size = _users.size();
	for (unsigned int i = 0; i < size; i++)
	{
		if (_users[i].second == usrNum)
		{
			_users.erase(_users.begin() + i);
			std::lock_guard<std::mutex> lck(mtxSocket);
			closesocket(_sockets[i]);
			_sockets.erase(_sockets.begin() + i);
			break;
		}
	}
}

void Server::addMsgsOfClient(SOCKET clientSocket, unsigned int usrNum)
{
	std::string msg(MSG_SIZE, 0);
	std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
	int recvReturnValue = 0;
	try
	{
		while (true)
		{
			if ((recvReturnValue = recv(clientSocket, &(msg[0]), MSG_SIZE, 0)) == 0 || recvReturnValue == SOCKET_ERROR)
			{
				break;
			}
			lck.lock();
			_msgs.push(std::pair<std::string, int>(msg, usrNum));
			lck.unlock();
			cv.notify_all();
		}
	}
	catch (...)
	{
	}
}
//acts according to the request.
void Server::handleMsgs()
{
	std::unique_lock<std::mutex> lckCv(mtxCv, std::defer_lock);
	std::unique_lock<std::mutex> lck(mtx, std::defer_lock);
	std::unique_lock<std::mutex> lckSocket(mtxSocket, std::defer_lock);
	std::ofstream ofs;
	int type = 0;//type of request
	int usrNum = 0;
	std::string currMsg;
	while (true)
	{
		if (!_msgs.size())
		{
			lckCv.lock();
			cv.wait(lckCv);
			lckCv.unlock();
		}
		else
		{
			lck.lock();
			currMsg = _msgs.front().first;
			usrNum = _msgs.front().second;
			lck.unlock();
			type = atoi(strSlicing(currMsg, 0, TYPE_LEN).c_str());//minimizing locked time
			switch (type)
			{//used numbers in the cases because those are clearer in this context then words - the message number was pre defined in the instructions
			case 200:
			{
				enum { SIZE_LEN = 2 };
				_users.push_back(std::pair<std::string, int>(strSlicing(currMsg, TYPE_LEN + SIZE_LEN, TYPE_LEN + SIZE_LEN +  atoi(&strSlicing(currMsg, TYPE_LEN , SIZE_LEN + TYPE_LEN)[0])), usrNum));//slicing the user's name and adding it to the queue
				sendOneOOneLastUser(_sockets.back());
			}
			break;
			case 204:
			{
				enum { SIZE_LEN = 5 };
				_data = strSlicing(currMsg, TYPE_LEN + SIZE_LEN, TYPE_LEN + SIZE_LEN+ atoi(&strSlicing(currMsg, TYPE_LEN, SIZE_LEN + TYPE_LEN )[0]));//slicing the data part, then adding it to the current data
				ofs.open("data.txt");
				ofs << _data;
				ofs.close();
				sendOneOOneToAll();
			}
			break;
			case 207:
			{
				enum { SIZE_LEN = 5 };
				_data = strSlicing(currMsg, TYPE_LEN + SIZE_LEN, TYPE_LEN + SIZE_LEN + atoi(&strSlicing(currMsg, TYPE_LEN, SIZE_LEN + TYPE_LEN)[0]));//slicing the data part, then adding it to the current data
				ofs.open("data.txt");
				ofs << _data;
				ofs.close();
				lckSocket.lock();//moving the current user (and his socket) to the back.
				movSocketToBack();
				lckSocket.unlock();
				movToBack();
				sendOneOOneToAll();
			}
				break;
			case 208:
			{
				lck.lock();
				int userNum = _msgs.front().second;
				lck.unlock();
				delUser(userNum);
				sendOneOOneToAll();
			}
			break;
			}
			lck.lock();
			_msgs.pop();
			lck.unlock();
		}
	}
}

